<?php
namespace App\Middleware;

class SecurityMiddleware extends \App\Middleware\BaseMiddleware{
	function handle($request) {
		if($request->keyExists('badParam')){
			unset($_REQUEST['badParam']);
			$request->removeParam('badParam');
		}
		return $request;
	}
}