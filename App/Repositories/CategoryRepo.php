<?php
namespace App\Repositories;
use App\Models\Category;

class CategoryRepo extends BaseRepo {
    protected $model = Category::class;

}