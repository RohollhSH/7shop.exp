<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class CommentController{

	public function index($request) {
		$data = [];
		View::load('panel.comment.index',$data,'panel-admin');
	}
	
}