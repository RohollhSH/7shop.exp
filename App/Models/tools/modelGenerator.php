<?php
$fileContent = "<?php
namespace App\Models;

class MODEL_NAME extends BaseModel {
	protected \$table = 'TABLE_NAME' ;
	protected \$primaryKey = 'id' ;

}";

$models = [
    'Payment' => 'payments',
    'User' => 'users',
    'Address' => 'addresses',
    'Order' => 'orders',
    'Product' => 'products',
    'Media' => 'media',
    'Review' => 'reviews',
    'Category' => 'categories',
    'Attribute' => 'attributes',
    'AttributeValue' => 'attribute_values',
];

foreach ($models as $modelName=>$tableName){
    $thisContent = str_replace(['MODEL_NAME','TABLE_NAME'],[$modelName,$tableName],$fileContent);
//    $thisContent = str_replace('TABLE_NAME',$tableName,$thisContent);
    file_put_contents("$modelName.php",$thisContent);
}
