<?php
namespace App\Models;

class User extends BaseModel {
	protected $table = 'users' ;
	protected $primaryKey = 'id' ;
    protected $guarded = ['id', 'role'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
}
