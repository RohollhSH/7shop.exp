<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= asset('img','icon.png') ?>">
    <title>پنل فروشگاه سون شاپ</title>
    <link href="<?= asset('css','lib/bootstrap/bootstrap-rtl.min.css') ?>" rel="stylesheet">
    <link href="<?= asset('css','helper.css') ?>" rel="stylesheet">
    <link href="<?= asset('css','fonts.css') ?>" rel="stylesheet">
    <link href="<?= asset('css','style.css') ?>" rel="stylesheet">
</head>
<body>
<!-- Preloader - style you can find in spinners.css -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <!-- header header  -->
    <div class="header">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- Logo -->
            <div class="navbar-header">
                <a class="<?= siteUrl('path') ?>">
                    <!-- Logo icon -->
                    <b><img src="../../assets/img/icon.png" alt="homepage" class="dark-logo"/></b>
                    <!--End Logo icon -->
                    <!-- Logo text -->
                    <span><img src="../../assets/img/logo-text.png" alt="homepage" class="dark-logo"/></span>
                </a>
            </div>
            <!-- End Logo -->
            <div class="navbar-collapse">
                <!-- toggle and nav items -->
                <ul class="navbar-nav mr-auto mt-md-0">
                    <!-- This is  -->
                    <li class="nav-item"><a class="nav-link nav-toggler hidden-md-up text-muted " href="<?= siteUrl('path') ?>"><i class="mdi mdi-menu"></i></a></li>
                    <li class="nav-item m-l-10"><a class="nav-link sidebartoggler hidden-sm-down text-muted" href="<?= siteUrl('path') ?>"><i class="ti-menu"></i></a></li>
                </ul>
                <!-- User profile and search -->
                <ul class="navbar-nav my-lg-0">

                    <!-- Notifications -->
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle text-muted text-muted " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i>
                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                            <ul>
                                <li>
                                    <div class="drop-title">اعلان ها</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-danger btn-circle m-r-10"><i class="fa fa-link"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>عنوان اعلان</h5> <span class="mail-desc">این متن اعلان است.</span>
                                                <span class="time">9:30 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-success btn-circle m-r-10"><i class="ti-calendar"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>عنوان اعلان</h5> <span class="mail-desc">این متن اعلان است.</span>
                                                <span class="time">9:10 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-info btn-circle m-r-10"><i class="ti-settings"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>عنوان اعلان</h5> <span class="mail-desc">این متن اعلان است.</span>
                                                <span class="time">9:08 AM</span>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="<?= siteUrl('path') ?>;"><i class="fa fa-angle-left"></i> <strong> نمایش همه اعلان ها</strong> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- End Notifications -->
                    <!-- Profile -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="../../assets/img/avatar.jpg" alt="user" class="profile-pic"/>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                            <ul class="dropdown-user">
                                <li><a href="#"><i class="ti-user"></i> پروفایل شما</a></li>
                                <li><a href="#"><i class="fa fa-power-off"></i> خروج</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- End header header -->
    <!-- Left Sidebar  -->
    <div class="right-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="nav-devider"></li>
                    <li class="nav-label"></li>
                    <li>
                        <a class="has-arrow " href="#" aria-expanded="false">
                            <i class="fa fa-pie-chart"></i> <span class="hide-menu">داشبورد</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?= siteUrl('panel/dashboard') ?>">آمار و اطلاعات </a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow  " href="#" aria-expanded="false">
                            <i class="fa fa-list-alt"></i> <span class="hide-menu">محصولات</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?= siteUrl('panel/products/all') ?>">لیست محصولات</a></li>
                            <li><a href="<?= siteUrl('panel/product/add') ?>">افزودن محصول</a></li>
                            <li><a href="<?= siteUrl('panel/media/add') ?>">افزودن تصاویر محصول</a></li>
                            <li><a href="<?= siteUrl('panel/comments') ?>">کامنت های محصولات</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="fa fa-tags"></i> <span class="hide-menu">دسته بندی ها</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?= siteUrl('panel/categories') ?>">دسته بندی ها</a></li>
                            <li><a href="<?= siteUrl('panel/attributes') ?>">ویژگی ها</a></li>
                            <li><a href="<?= siteUrl('panel/categories/attributes') ?>">دسته بندی <=> ویژگی</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="fa fa-shopping-cart"></i> <span class="hide-menu">سفارش ها</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?= siteUrl('panel/orders') ?>">لیست سفارش ها</a></li>
                            <li><a href="<?= siteUrl('panel/shipments') ?>">ارسال ها</a></li>
                            <li><a href="<?= siteUrl('panel/payments') ?>">پرداخت ها</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">
                            <i class="fa fa-users"></i> <span class="hide-menu">کاربر</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?= siteUrl('panel/users') ?>">لیست کاربران</a></li>
                            <li><a href="<?= siteUrl('panel/users/add') ?>">کاربر جدید</a></li>
                            <li><a href="<?= siteUrl('panel/user/profile') ?>">پروفایل کاربری</a></li>
                            <li><a href="<?= siteUrl('panel/user/addresses') ?>">آدرس ها</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow" href="#" aria-expanded="false">
                            <i class="fa fa-cog"></i> <span class="hide-menu">تنظیمات</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?= siteUrl('panel/options/add') ?>">افزودن</a></li>
                            <li><a href="<?= siteUrl('panel/options') ?>">همه تنظیمات</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </div>
    <!-- End Left Sidebar  -->
    <?php echo $view; ?>
</div>
<!-- End Wrapper -->

<?php \App\Services\Flash\FlashMessage::show_messages(); ?>


<script src="<?= asset('js','lib/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?= asset('js','popper.min.js') ?>"></script>
<script src="<?= asset('js','bootstrap.min.js') ?>"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?= asset('js','jquery.slimscroll.js') ?>"></script>
<!--Menu sidebar -->
<script src="<?= asset('js','sidebarmenu.js') ?>"></script>
<!--stickey kit -->
<script src="<?= asset('js','lib/sticky-kit-master/dist/sticky-kit.min.js') ?>"></script>
<script src="<?= asset('js', 'lib/chart-js/Chart.bundle.js') ?>"></script>
<script src="<?= asset('js','lib/chart-js/chartjs-init.js') ?>"></script>

<!--Custom JavaScript -->
<script src="<?= asset('js','custom.min.js') ?>"></script>
</body>
</html>